echo off

NET SESSION >nul 2>&1
IF %ERRORLEVEL% EQU 0 (
    GOTO RUNNER
) ELSE (
    GOTO CHECKADMIN
)

:CHECKADMIN
setlocal DisableDelayedExpansion
set "filePath=%~0"
for %%k in (%0) do set fileName=%%~nk
set "vbsPriv=%temp%\OEgetPriv_%fileName%.vbs"
setlocal EnableDelayedExpansion
NET FILE 1>NUL 2>NUL
if '%errorlevel%' == '0' (goto ISADMIN) else (goto ESCALATE)

:ESCALATE
if '%1'=='ELEV' (echo ELEV & shift /1 & goto ISADMIN)
ECHO Set userAccCntrl = CreateObject^("Shell.Application"^) > "%vbsPriv%"
ECHO args = "ELEV " >> "%vbsPriv%"
ECHO For Each strArg in WScript.Arguments >> "%vbsPriv%"
ECHO args = args ^& strArg ^& " "  >> "%vbsPriv%"
ECHO Next >> "%vbsPriv%"
ECHO userAccCntrl.ShellExecute "!filePath!", args, "", "runas", 1 >> "%vbsPriv%"
"%SystemRoot%\System32\WScript.exe" "%vbsPriv%" %*
exit /B

:ISADMIN
setlocal & pushd .
cd /d %~dp0
if '%1'=='ELEV' (del "%vbsPriv%" 1>nul 2>nul  &  shift /1)
REM cmd /k

:RUNNER
set "progPath=%~dp0"
start "eLuo Software" "%progPath%eLuo Software.exe"
:END
exit
